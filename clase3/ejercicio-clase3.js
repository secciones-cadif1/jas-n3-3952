$(document).ready(()=>{
    let lista;
    try{
        lista = JSON.parse(localStorage.getItem("lista"))
        for (i=0;i<lista.length;i++)
            agregarItemLista(lista[i])

    }catch(e){
        lista = []
    }

    $("#tarea").on("keypress",(event)=>{
        //console.log(event)
        // cuando el usuario presione enter
        if (event.originalEvent.keyCode == 13){
            
            let tarea = $("#tarea").val()
            if (lista.indexOf(tarea)!=-1)
                alert("ya esta en la lista")
            else{
                lista.push(tarea)
                agregarItemLista(tarea)

                localStorage.setItem("lista",JSON.stringify(lista))
                
                $("#tarea").val("")
            }
        }
    })

    function agregarItemLista(texto){
        let li=document.createElement("li")
        li.innerText = texto

        li.onclick=()=>{
            let n = lista.indexOf(texto)
            $(li).remove()
            lista.splice(n,1)
            localStorage.setItem("lista",JSON.stringify(lista))
        }
        $("#lista").append(li)
    }

    $("#limpiar").click(()=>{
        $("#lista").empty()
        lista=[]
        localStorage.setItem("lista",JSON.stringify(lista))
    })
})