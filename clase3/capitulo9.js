
// cuando la pagina este lista, equivalente al onload
jQuery(document).ready(()=>{
    let contador=localStorage.getItem("contador");
    $("#descontar").hide()
    

    if (contador == undefined)
        contador = 0;

    document.getElementById("contador").innerText = contador;
    localStorage.setItem("contador",contador)

    $("#contar").click(function(){
        $(this).hide()
        $("#descontar").show()
        contador++;
        localStorage.setItem("contador",contador)
        document.getElementById("contador").innerText = contador;
    })

    // se vuelve a habilitar el boton
    document.getElementById("contar").disabled=false;

    $("#descontar").click(function(){
        $(this).hide()
        $("#contar").show()
    })
})
